# Generated by Django 4.0.3 on 2023-01-20 21:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_alter_shoe_wardrobe_bin'),
    ]

    operations = [
        migrations.RenameField(
            model_name='binvo',
            old_name='section_number',
            new_name='bin_number',
        ),
        migrations.RenameField(
            model_name='binvo',
            old_name='shelf_number',
            new_name='bin_size',
        ),
    ]
